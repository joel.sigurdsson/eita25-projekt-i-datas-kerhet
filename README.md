# EITA25 Projekt i datasäkerhet

Projekt i kursen EITA25 Datasäkerhet.

## Del 1.

#### 1.
Run the command openssl req -newkey rsa:2048 -nodes -keyout privateCAkey.pem -x509 -days 365 -out publicCAkey.pem

-CAcreateserial:  With this option the CA serial number file is created if it does not exist: it will contain the serial number "02" and the certificate being signed will have the 1 as its serial number. If the -CA option is specified and the serial number file does not exist a random number is generated; this is the recommended practice.

#### 2.
keytool -import -file publicCAkey.pem -alias CA -keystore NAMETrustStore

#### 3.
keytool -genkey -keyalg RSA -alias {NAME}KS -keystore NAMEkeystore

#### 4.
keytool -certreq -keystore NAMEkeystore -alias {NAME}KS -keyalg rsa -file NAMErequest.csr

#### 5.
openssl x509 -req -CA publicCAkey.pem -CAkey privateCAkey.pem -in NAMErequest.csr -out NAMEsigned.cer -days 365 -CAcreateserial

#### 6.
keytool -import -keystore NAMEkeystore -file publicCAkey.pem -alias CA

keytool -import -keystore NAMEkeystore -file NAMEsigned.cer -alias NAMEcertificate

#### 7.
..

#### 8.
When the key is signed, use -extfile v3.ext.
  B, Looks like:
  >authorityKeyIdentifier=keyid,issuer
  basicConstraints=CA:FALSE
  keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
  C, Additional information, such as max length of chain, alternative name of subject etc (optional)

#### 9.
