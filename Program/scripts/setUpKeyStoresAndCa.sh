# PRO TIP! Run as ./clean.sh; ./setUpKeyStoresAndCa.sh | grep "entries"
#
# Creates the public and private key pair for Certfification Autorithy, CA
# Should generate two files
openssl req -x509 -newkey rsa:2048 -nodes -keyout private_CA_key.pem -out public_CA_key.pem -days 365 < ./inFiles/createCA.in
#
# Signs the root certificate, generates clientTrustStore
echo 'y' | keytool -import -file public_CA_key.pem -alias CA -keystore clientTrustStore -storepass password
#
# ---------- CLIENT  -----------
# Creates clientKeyStore.
keytool -keystore clientKeyStore -genkey -alias CKS -storepass password < ./inFiles/clientcert.in
#
# Create the request for client to get certified. Generates one file
keytool -keystore clientKeyStore -certreq -alias CKS -keyalg rsa -file clientRequest.csr -storepass password
#
# Signs the request.
openssl x509 -req -CA public_CA_key.pem -CAkey private_CA_key.pem -in clientRequest.csr -out clientSigned.cer -days 365 -CAcreateserial
#
#
echo 'y' | keytool -import -keystore clientKeyStore -file public_CA_key.pem -alias CA -storepass password
#
#
echo 'y' | keytool -import -keystore clientKeyStore -file clientSigned.cer -alias CKS  -storepass password
#
# Examine client keystore.
echo
echo  "-------------CLIENT DONE! - CHECKING CHAIN-----------------"
echo
echo "password" | keytool -keystore clientKeyStore -list -v


# ---------- SERVER -----------
# Creates serverKeyStore.
keytool -keystore serverKeyStore -genkey -alias SKS -storepass password < ./inFiles/servercert.in
#
# Create the request for client to get certified. Generates one file
keytool -keystore serverKeyStore -certreq -alias SKS -keyalg rsa -file severRequest.csr -storepass password
#
# Signs the request.
openssl x509 -req -CA public_CA_key.pem -CAkey private_CA_key.pem -in severRequest.csr -out serverSigned.cer -days 365 -CAcreateserial
#
#
echo 'y' | keytool -import -keystore serverKeyStore -file public_CA_key.pem -alias CA -storepass password
#
#
echo 'y' | keytool -import -keystore serverKeyStore -file serverSigned.cer -alias SKS  -storepass password
#
# Examine client keystore.
echo
echo  "-------------SERVER DONE! - CHECKING CHAIN-----------------"
echo
echo "password" | keytool -keystore serverKeyStore -list -v
